<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Factory\ApiTokenFactory;
use App\Factory\DragonTreasureFactory;
use App\Factory\UserFactory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

final class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        UserFactory::createOne([
            'email' => 'bernie@dragonmail.com',
            'password' => 'roar',
        ]);

        UserFactory::createMany(10);
        DragonTreasureFactory::createMany(40, static fn () => [
            'isPublished' => random_int(0, 10) > 3,
            'owner' => UserFactory::random(),
        ]);

        ApiTokenFactory::createMany(30, static fn (): array => [
            'ownedBy' => UserFactory::random(),
        ]);
    }
}
