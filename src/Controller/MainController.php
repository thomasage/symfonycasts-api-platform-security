<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\CurrentUser;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

final class MainController extends AbstractController
{
    public function __construct(private readonly NormalizerInterface $normalizer)
    {
    }

    #[Route('/')]
    public function homepage(#[CurrentUser] User $user = null): Response
    {
        return $this->render('main/homepage.html.twig', [
            'userData' => $this->normalizer->normalize($user, 'jsonld', ['groups' => 'user:read']),
        ]);
    }
}
